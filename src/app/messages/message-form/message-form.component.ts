import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';




@Component({
  selector: 'messageForm',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {
  @Output()
  addMessage: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  addMessagePs: EventEmitter<any> = new EventEmitter<any>();
  
  service:MessagesService;
  msgform = new FormGroup({
    message: new FormControl(),
    user: new FormControl()
  });

  sendData(){
    this.addMessage.emit(this.msgform.value.message);
    this.service.postMessage(this.msgform.value).subscribe(response =>{
      this.addMessagePs.emit();  
      console.log(response);
    });
  }


  constructor(service:MessagesService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}
