
import {environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams} from '@angular/common/http';
import 'rxjs/Rx';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class MessagesService {

  url = environment.url;
  getMessages(){
    //return ['course1', 'course2', 'course3'];
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({'Authorization': 'Bearer '+token}
    )};  
    return this.http.get(this.url +'messages', options);
  }

  getMessagesFire(){
    return this.db.list('/messages').valueChanges();
  }

  getMessage(id){
    //return ['course1', 'course2', 'course3'];
    return this.http.get(this.url + 'messages/'+id);
  }
  postMessage(data){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded','Authorization': 'Bearer '+token }
    )};
    var params = new HttpParams().append('message',data.message).append('user',data.user);
    return this.http.post(this.url + 'messages',params.toString(),options); 
  }

  deleteMessage(id){
    return this.http.delete(this.url + 'messages/'+id); 
  }


  login(credentials){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post(this.url + 'auth',params.toString(),options).map(response=>{
      console.log(this.url);
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);
    })
  }


  constructor(private http:Http, private db:AngularFireDatabase ) { 
    
  }
  

}
