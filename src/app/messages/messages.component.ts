import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  title = "List of messages";
  messages;
  messagesKeys = []; 
  //messages = ['Message1','Message2','Message3'];
  
  getTitle(){
    return this.title;
  }

  optimisticAdd(message){
    var newKey = this.messagesKeys[this.messagesKeys.length-1]+1;
    var newMessageObject = {};
    newMessageObject['body'] = message;
    this.messages[newKey] = newMessageObject;
    this.messagesKeys = Object.keys(this.messages);
  }

  pessimiaticAdd(){
    this.service.getMessages().subscribe(response => {
      this.messages =  response.json();
      this.messagesKeys = Object.keys(this.messages);
    });   
  }

  deleteMessage(key){
    console.log(key);
    let index = this.messagesKeys.indexOf(key);
    this.messagesKeys.splice(index,1);
    this.service.deleteMessage(key).subscribe(response =>console.log(response));
  }

  

  constructor(private service:MessagesService) {
        this.service.getMessages().subscribe(response => {
          this.messages =  response.json();
          this.messagesKeys = Object.keys(this.messages);
        });
  }

  ngOnInit() {
  }

}
