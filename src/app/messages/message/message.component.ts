import { MessagesService } from './../messages.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message;
  constructor(private route:ActivatedRoute, private service:MessagesService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params =>{
      console.log(params);
      var id = params.get('id');
      this.service.getMessage(id).subscribe(response =>{
        console.log(response)
        this.message = response.json();
        console.log(this.message.id)
      })
    })
  }

}
