import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fire-messages',
  templateUrl: './fire-messages.component.html',
  styleUrls: ['./fire-messages.component.css']
})
export class FireMessagesComponent implements OnInit {

  messages;
  constructor(private service:MessagesService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(messages =>{
      this.messages = messages;
      console.log(this.messages);
    });

  }

}
