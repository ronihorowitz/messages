
import { MessagesService } from '../messages/messages.service';
import {FormGroup, FormControl} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  invalid = false; 

  loginForm = new FormGroup({
    user: new FormControl(),
    password: new FormControl()
  });


  login(){
    this.service.login(this.loginForm.value).subscribe(response =>{
      this.router.navigate(['/']); 
    }, error =>{
      this.invalid = true; 
    });
  }

  logout(){
    console.log("logout clicked");
    this.invalid = false; 
    localStorage.removeItem('token');
  }


  constructor(private router:Router,private service:MessagesService) { 

  }

  ngOnInit() {
  }

}



